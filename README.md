# gulp-autocrop

Based on https://github.com/odrick/gulp-images-resizer so all credit to odrick, however I didn't want to rewrite his library to conform to latest jimp so made my own.

It will only resize and crop your image any quality adjustments need to be done in another gulp plugin such as imagemin.


### Install
```bash
npm install gulp-autocrop
```

### Usage
```js
const { src, dest } = require('gulp');
const autocrop = require('gulp-autocrop');


function imageTask (cb) {
    src('src/*.png')
        .pipe(autocrop({
            ratio: '16:9'
        }))
        .pipe(dest('output'))
        .on('finish', cb);
};

exports.default = imageTask;
```

### Options

Must have at least one of the following options:

* `ratio` - `string` 16:9, 2:1 whatever you fancy.
* `width` - `int` in pixels only.
* `height` - `int` in pixels only.
* `maintainAspectRatio` - `boolean` default true maintain structure of the image if height or width is auto