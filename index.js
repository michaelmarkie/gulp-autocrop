const 
	through = require('through2'),
	Jimp = require('jimp'),
	app = require('./package.json'),
	SUPPORTED_EXT = ['png', 'jpg', 'jpeg', 'gif', 'bmp'];


function error (txt, cb) {
	console.error(app.name + ': ' + txt);
	return cb();
}


function autoCrop (o = {}) {

	if( Object.keys(o).length === 0 ) return error('Need at least one option [ratio,width,height]', cb);

	o.width = o.width || Jimp.AUTO;
	o.height = o.height || Jimp.AUTO;

	o.maintainAspectRatio = o.maintainAspectRatio || true;

	o.ratio = o.ratio || false;

	function bufferContents (file, enc, cb) {

		const ext = file.relative.split('.').pop();

		if (file.isNull())
			return cb();

		if (file.isStream())
			return error('Streaming not supported', cb);

		if ( ! SUPPORTED_EXT.includes(ext) )
			return cb();

		Jimp.read(file.contents, (err, image) => {

			if ( err ) return error('Error reading ' + file.relative, cb);


			// Maintain picture aspect
			if( o.maintainAspectRatio && ! o.ratio ) {

				// Calculate height based on width
				if ( o.width != Jimp.AUTO && o.height === Jimp.AUTO ) 
					o.height = image.bitmap.height / (image.bitmap.width / o.width);
				
				// Calculate width based on height
				if ( o.height != Jimp.AUTO && o.width === Jimp.AUTO ) 
					o.width = image.bitmap.width / (image.bitmap.height / o.height);

			}

			// Cover cant handle auto so we'll set the sizes manually
			if ( o.width === Jimp.AUTO )
				o.width = image.bitmap.width;

			if ( o.height === Jimp.AUTO )
				o.height = image.bitmap.height;




				
			if ( o.ratio && typeof o.ratio === 'string' ) {

				// Check ratio is valid num:num
				if ( ! /^[0-9.]+:[0-9.]+$/g.test(o.ratio) )
					return error('Ratio invalid', cb);

				// Split the ratio into an array and parseInt values.
				let ratio = o.ratio.split(':').map(v => parseInt(v));

				// Find the shortest side to work from, so 16:9 will crop the height.
				if ( ratio[0] >= ratio[1] )
					o.height = ( o.width / ratio[0] ) * ratio[1];
				else
					o.width = ( o.height / ratio[1] ) * ratio[0];

			}

			
			// Image is size we expect already
			if ( o.width == image.bitmap.width && o.height == image.bitmap.height ) {
				this.push(file);
				return cb();
			}


			image.cover(o.width, o.height);


			const format = (( format ) => {
				switch (format) {
					case 'png':
						return Jimp.MIME_PNG;
					case 'gif':
						return Jimp.MIME_GIF;
					case 'bmp':
						return Jimp.MIME_BMP;
					default:
						return Jimp.MIME_JPEG;
				}
			})(ext);

			image.getBuffer(format, (err, buffer) => {
				
				if (err) return error('Error getting buffer ' + file.relative, cb);

				// Update pipeline
				file.contents = buffer;
				this.push(file);
				cb();

			});
		});
	}

	return through.obj(bufferContents, cb => cb());
};


module.exports = autoCrop;
