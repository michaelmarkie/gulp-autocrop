const { src, dest, series } = require('gulp');
const autocrop = require('./index');
const rename = require('gulp-rename');


function w400Task (cb) {

    src('test/*.jpg')
        .pipe(autocrop({
            width: 400
        }))
        .pipe(rename({
            basename: 'w400'
        }))
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

function w400h400Task (cb) {

    src('test/*.jpg')
        .pipe(autocrop({
            width: 400,
            height: 400
        }))
        .pipe(rename({
            basename: 'w400_h400'
        }))
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

function ratioTask (cb) {

    src('test/*.jpg')
        .pipe(autocrop({
            ratio: '1:1'
        }))
        .pipe(rename({
            basename: '1:1'
        }))
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

function ratioWidthTask (cb) {

    src('test/*.jpg')
        .pipe(autocrop({
            ratio: '1:1',
            width: 100
        }))
        .pipe(rename({
            basename: '1:1_w100'
        }))
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

function weirdRatioTask (cb) {

    src('test/*.jpg')
        .pipe(autocrop({
            ratio: '50:6'
        }))
        .pipe(rename({
            basename: '50:6'
        }))
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

exports.default = series(w400Task, w400h400Task, ratioTask, ratioWidthTask, weirdRatioTask);
